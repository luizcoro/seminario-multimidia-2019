#!/bin/bash

directories=$@
codecs=(libx264 libx265)
qualities=(1 6 11 16 21 26 31 36 41 46 51)

for dir in $directories; do
    for codec in ${codecs[@]}; do
        for quality in ${qualities[@]}; do
            # call ffmpeg to compress the reference videos
            ffmpeg -i "$dir/reference.y4m" \
                -vcodec $codec -crf $quality "$dir/$codec-$quality.mp4"
        done
    done
done

