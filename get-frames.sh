#!/bin/bash

# directories=${@:1:$#-1}
# amount="${!#}"

for dir in "$@"; do
    # get video duration of file reference.y4m
    # duration_float=$(ffprobe -i "$dir/reference.y4m" \
    #     -show_entries format=duration -v quiet -of csv="p=0")
    # duration_int=${duration_float%.*}

    # generate random seconds between 0-$duration
    # declare -a random_seconds
    # for i in $(seq 1 $amount); do
    #     random_seconds[$i]=$(seq 0 .01 $duration_int | shuf | head -n1)
    # done

    for file in $dir/*; do
        # create a new dictory with same name of $file
        filename=$(basename $file)
        filename="${filename%.*}" # remove extension
        mkdir "$dir/$filename"

        # for i in $(seq 1 $amount); do
        #     # fast forward to $time_to_forward and take a single frame
        #     ffmpeg -i $file \
        #         -ss ${random_seconds[$i]} -vframes 1 "$dir/$filename/$i.bmp"
        # done

        ffmpeg -i $file "$dir/$filename/%06d.bmp"
    done
done

