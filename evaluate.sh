#!/bin/bash

directories=${@:1:$#-1}
approach="${!#}"

# rm -f "${approach}-results.csv"

# if [[ $approach == "nr" ]]; then
#     echo "video,codec,quality,frame,y" >> "${approach}-results.csv"
# else
#     echo "video,codec,quality,frame,rmse,ssim,psnr,y" >> "${approach}-results.csv"
# fi

for dir in ${directories[@]}; do
    video_name=$(basename $dir)
    for encoder_dir in $dir/*; do
        if [[ -d $encoder_dir ]]; then
            encoder_name=$(basename $encoder_dir)
            encoder_quality="${encoder_name##*-}"
            encoder_name="${encoder_name%-*}"

            if [[ $encoder_name == $encoder_quality ]]; then
                encoder_quality=0
            fi

            for frame in $encoder_dir/*; do
                frame_number=$(basename $frame)
                frame_number="${frame_number%.*}" # remove extension
                reference="$dir/reference/$frame_number.bmp"

                if [[ $approach == "nr" ]]; then
                    reference=""
                fi

                # print commands too
                echo "python authors-code/evaluate.py" \
                    "--model authors-code/models/${approach}_tid_weighted.model" \
                    "--top weighted $frame $reference"
                result=$(python authors-code/evaluate.py \
                    --model "authors-code/models/${approach}_tid_weighted.model" \
                    --top weighted $frame $reference)

                if [[ $approach == "nr" ]]; then
                    echo "$video_name,$encoder_name,$encoder_quality,$frame_number,$result" >> "${approach}-results.csv"
                else
                    rmse=$(magick compare -metric rmse $frame $reference error.png 2>&1)
                    rmse=$(echo $rmse | cut -d "(" -f2 | cut -d ")" -f1)

                    ssim=$(magick compare -metric ssim $frame $reference error.png 2>&1)
                    psnr=$(magick compare -metric psnr $frame $reference error.png 2>&1)
                    echo "$video_name,$encoder_name,$encoder_quality,$frame_number,$rmse,$ssim,$psnr,$result" >> "${approach}-results.csv"
                fi
            done
        fi
    done
done

rm -f error.png
