\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{hyperref}
\usepackage{listings}

\ifCLASSOPTIONcompsoc
    \usepackage[caption=false, font=normalsize, labelfont=sf, textfont=sf]{subfig}
\else
    \usepackage[caption=false, font=footnotesize]{subfig}
\fi

\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

\lstset{
  basicstyle=\footnotesize,
  breaklines=true,
  % frame=single,
  keepspaces=true,
  language=bash,
  aboveskip=15pt,
  belowskip=15pt
  % title=~,
}

\begin{document}


\title{Using CNNs for Quality Assessment of No-Reference and Full-Reference Compressed-Video Frames\\}

\author{
\IEEEauthorblockN{Renato Rodrigues da Silva}
\IEEEauthorblockA{School of Computer Science\\
Federal University of Uberl\^{a}ndia\\
Uberl\^{a}ndia, MG, Brazil\\
Email: renato.rsufu@gmail.com}
\and
\IEEEauthorblockN{Luiz Fernando Afra Brito}
\IEEEauthorblockA{School of Computer Science\\
Federal University of Uberl\^{a}ndia\\
Uberl\^{a}ndia, MG, Brazil\\
Email: luiz.brito@ufu.br}
\and
\IEEEauthorblockN{Marcelo Keese Albertini}
\IEEEauthorblockA{School of Computer Science\\
Federal University of Uberl\^{a}ndia\\
Uberl\^{a}ndia, MG, Brazil\\
Email: albertini@ufu.br}
\and
\IEEEauthorblockN{Marcelo Zanchetta do Nascimento}
\IEEEauthorblockA{School of Computer Science\\
Federal University of Uberl\^{a}ndia\\
Uberl\^{a}ndia, MG, Brazil\\
Email: marcelo.nascimento@ufu.br}
\and
\IEEEauthorblockN{Andr\'e R. Backes}
\IEEEauthorblockA{School of Computer Science\\
Federal University of Uberl\^{a}ndia\\
Uberl\^{a}ndia, MG, Brazil\\
Email: arbackes@yahoo.com.br}
}


\maketitle

\begin{abstract}
    The consume of digital contents, especially digital video streaming, is increasingly becoming part of our everyday lives. For videos to be streamed, they have to be coded and sent to users as signals that are decoded back to be reproduced. This coding-decoding process may result in distortion that can bring differences in the quality perception of the content, consequently, influencing user experience. The approach proposed by Bosse et al.~\cite{journals/tip/BosseMMWS18} suggests an Image Quality Assessment (IQA) method using an automated process. They use image datasets pre-labeled with quality scores to perform a Convolutional Neural Network (CNN) training. Then, based on the CNN models, they are able to perform predictions of image quality using both Full-Reference (FR) and No-Reference (NR) evaluation. Although the proposed methods aimed video quality assessment, the training and tests performed in the original work are only done using single images. In this paper, we explore these methods exposing the CNN quality prediction to images extracted from actual videos. Various quality compression levels were applied to them as well as two different video codecs. We also evaluated how their models perform while predicting human visual perception of quality in scenarios where there is no human pre-evaluation, observing its behaviour along with metrics such as SSIM and PSNR. We observe that FR model is able to better infer human perception of quality for compressed videos. Differently, NR model does not show the same behaviour for most of the evaluated videos.

\end{abstract}

\begin{IEEEkeywords}
convolutional neural networks, image quality assessment, perceptual quality, video compression.
\end{IEEEkeywords}

\section{Introduction}

The consume of digital contents is increasingly becoming part of our everyday lives, especially regarding digital video. Video streaming is widely transmitted nowadays not only for high definition television, but also for video chats, conferences and internet streaming~\cite{journals/tip/BosseMMWS18}.

For video contents to be transmitted to users, some important processes must be done. First, the video content has to be coded and transformed in signals that will be sent as packages to the final user~\cite{fundamentalsofMultimedia}. Next, this signals are decoded and remounted as video content to be reproduced on the user side. The problem is that this coding-decoding process may result in distortions which may lead to differences in the quality perception. Therefore, the received video, when reproduced to the audience, may not show the exact quality as the original source file~\cite{fundamentalsofMultimedia}.

Depending on the context of video reproduction, quality assessment can be a crucial aspect. Usually, the quality of a video is related to the quality of the images or frames that compose it. The human perception of the quality of a visual content can be hard to quantify as it is a subjective matter and may vary from person to person. Thus, being able to assess quality is an important task but, definitely not a trivial one~\cite{Wang}. One of the ways to perform Image Quality Assessment (IQA) is to make a classification depending on the amount of information from the original reference image present in the distorted one~\cite{journals/tip/BosseMMWS18}. When the access to the full reference image is available, the IQA approach is called Full-Reference (FR), and, when it is not available, the IQA is called No-Reference (NR) approach~\cite{journals/tip/BosseMMWS18}.

Some state-of-the-art techniques evaluate quality of images and videos purely based on human opinion. Basically, various samples of images with different levels and types of compression are shown to human subjects that, based on their visual perception, classify the samples with scores of quality~\cite{Keng}.

Recently, other approaches have been proposed to perform IQA using automated methods. For example, in the work of Bosse et al.~\cite{journals/tip/BosseMMWS18}, the authors use datasets of images already classified according to their quality to train a Convolutional Neural Network (CNN).  Classification was performed with traditional human review and used as training and validation samples to the CNN. Then, the CNN models were used to perform predictions of Image Quality Assessment.

Convolutional neural networks, in recent years, have shown great relevance among the traditional approaches related to computer vision. This technique has been widely used due to the quality and amount of data available, and the computing power that has been growing significantly through the years. Furthermore, CNNs allow researchers to provide joint learning of resources and regression based on raw input data with very little manual interference needed~\cite{oai:arXiv.org:1409.1556}.

These networks receive labeled samples as inputs. As these samples pass through the network layers by the epochs, features are extracted and the network learns, more generally, which features best represent each label ~\cite{KangYLD14}. Different types of layers can be used to build the network structure. Some of the most commonly used are the convolutional layer, the pooling layer and the fully connected layer. The convolutional layer is responsible for applying convolutions using activation filter masks responsible for extracting the features of the image samples. The use of this type of layer is the reason for the name ``convolutional neural network''. The filters are initially defined in a random way and have their values adjusted gradually at each iteration of the samples in the neural network~\cite{KangYLD14}. The pooling layer is responsible for receiving samples and, based on some parameters, producing smaller samples which occupy less disk space. This fact is important since neural networks usually demand a large amount of input samples. Besides this advantage, this layer is intended to generate more robust features by reducing the sensitivity of the network to distortions. This way, a greater variety of images can be associated with the generated features, thus enhancing the classification~\cite{KangYLD14}. Finally, the fully connected layer is responsible for performing regression and weight adjustments. The samples used as inputs to the neural network are initially divided into training and validation sets. Then, the validation set is compared with the training set in order to identify necessary weight adjustments for next iterations~\cite{KangYLD14}.

In their work, Bosse et al.~\cite{journals/tip/BosseMMWS18} use TID2013~\cite{tid2013} and LIVE~\cite{live} datasets of images already classified according to their quality. The quality labels previously defined by human subjects are used as classes to train a CNN using $3000$ epochs, $10$ convolutional layers, $5$ pooling layers, as well as $2$ fully connected layers for regression. After the training process, the CNN models are used to perform predictions of Image Quality Assessment. Also, it is worth mentioning that, although the aim of their work was to propose methods for assessment of image quality in video streaming, all images used in the training process were single pictures and not extractions of compressed videos. Thus, we can consider that compression methods covered by the training process only exploit spatial redundancies to reduce the size of pictures. Another approach could also exploit temporal redundancies when considering a sequence of pictures as frames that compose the video. Besides, the tests provided by their original article only states the use of images as tests to the CNN, in order to obtain quality prediction and compare that result to the quality evaluation provided by the human subjects in the dataset.

In this paper, we explore the methods created by Bosse et al.~\cite{journals/tip/BosseMMWS18} exposing the CNN quality prediction to frames extracted from real compressed videos. Two different video codecs using various quality compression levels were applied to them. We also evaluate how their methods perform while predicting human visual perception of quality in scenarios where there is no human pre-evaluation, observing its behaviour along with metrics such as SSIM and PSNR.

The remainder of this paper is organised as follows. In Section~\ref{sec:method} we describe the process of acquisition, compression and extraction of frames used in the test process, as well as the basic application of these test samples in the CNN using models previously trained. Section~\ref{sec:experiment} describes the experiments and we report the results achieved by this work. Conclusions and future work are presented in Section~\ref{sec:conclusion}.


\section{Methods}%
\label{sec:method}


In this section we introduce our methods used to investigate whether the CNN models proposed in~\cite{journals/tip/BosseMMWS18} infer correctly the perceptual quality of actual compressed videos.
First, in Section~\ref{sub:Video Acquisition} we present the raw videos used for compression.
Then, in Sections~\ref{sub:Video Compression} and~\ref{sub:Frame Extraction} we describe the steps to compress and extract frames from the raw videos.
Finally, in Section~\ref{sub:Perceptual Quality Inference} we detail the process of evaluation using the inference models. A flow chart of all the steps can be observed in Figure \ref{fig:flowChart}. Also, the overall configurations of our methods for this paper is presented at Table \ref{tab:configurations}.

\begin{figure}[ht]
\centering
 \includegraphics[width=0.38\textwidth]{images/fluxogram.png}
 \caption{ Flow chart showing the main steps of this project.}
  \label{fig:flowChart}
\end{figure}

\subsection{Video Acquisition}%
\label{sub:Video Acquisition}

First, we collected videos stored in raw formats that represent diverse scenarios.
This diversity is important because different characteristics can introduce different challenges for video codecs and IQA models.
For this paper, we obtained four 720p videos with 50 frames per second and no chroma sub sampling. We chose these videos because their quality and sub sampling level allowed us to do a more detailed evaluation. As they have high quality we could explore more freely more levels of compressions. Also, these videos have no copyright restrictions and are available on~\url{https://media.xiph.org/video/derf/}~\cite{xiph}.

\begin{figure}
    \centering
    \subfloat[ducks\label{fig:ducks}]{%
    \includegraphics[width=0.85\columnwidth]{images/ducks.pdf}}
    \\
    \subfloat[house\label{fig:house}]{%
    \includegraphics[width=0.85\columnwidth]{images/house.pdf}}
    \\
    \subfloat[park\label{fig:park}]{%
    \includegraphics[width=0.85\columnwidth]{images/park.pdf}}
    \\
    \subfloat[town\label{fig:town}]{%
    \includegraphics[width=0.85\columnwidth]{images/town.pdf}}
    \caption{Preview of the videos obtained for compression, frame extraction, and model evaluation.} \label{fig:videos}
\end{figure}

In Figure \ref{fig:videos}, we present the preview for the four videos we used to compress and evaluate the CNN models proposed in~\cite{journals/tip/BosseMMWS18}.
Figure~\ref{fig:ducks}, \texttt{ducks}, shows a frame extracted from the corresponding video that contains ducks swimming in a river.
This scene has slow movements and the colours have low frequencies.
The most challenging part for codecs to handle is the wave movements created by the ducks.
Figure~\ref{fig:house}, \texttt{house}, shows the landscape of a house surrounded by vegetation.
This scene has elements with low frequency -- the house -- and high frequency -- the vegetation.
Trees have borders with irregular shapes, which will probably affect negatively the compression in these regions.
Figure~\ref{fig:park}, \texttt{park}, shows people running with distinct clothing in a park.
The main characteristic of this scene is the fast movement of the objects.
While camera follows people, trees appear and disappear in the foreground and background, creating a not straightforward time dependency among objects.
Finally, Figure~\ref{fig:town}, \texttt{town}, shows the aerial view of a town.
This scene is very detailed and presents very high frequencies, a difficult scenario for video codecs.


\subsection{Video Compression}%
\label{sub:Video Compression}

We compressed the raw videos collected in the previous step using two video codecs provided by the FFmpeg software~\cite{ffmpeg}, namely, h.264~\cite{h264} and h.265~\cite{h265}.
These two video codecs reduce the size of raw videos by exploiting spatial and temporal redundancies.
First, they convert the image colour space to YCbCr and apply chroma sub sampling to reduce the size of each frame by half without perceptual degradation.
This is due to our vision system that do not distinguish subtle changes of colours.
Then, they apply prediction techniques to infer whole blocks of pixels by using data of other blocks previously processed.
Some techniques predict blocks using only data contained in the same frame, this is called spatial prediction~\cite{jpeg}.
Frames, such as those containing blue skies, can have well defined patterns and algorithms can use knowledge gathered previously to predict next blocks.
Spatial prediction techniques are also employed by image codecs such as JPEG~\cite{jpeg} and JPEG2000~\cite{jpeg2000}, which are used in the work of Bosse et al.~\cite{journals/tip/BosseMMWS18}.
Other techniques use data from other frames in order to predict next blocks, this is called temporal prediction~\cite{h265}.
For example, in a movie that contains a ball bouncing on the floor, blocks in the next frame can be predicted by observing the displacement of objects compared to their location in previous frames.
Therefore, compression of videos can have different results when compared with compression of single images due to the addition of temporal prediction techniques.

% >>>>>>>>(((talvez seja bom adicionar esta parte pra complementar:))))):
% What does not happen with single mages are compressed. That is the main reason whey we decided to do this work so that we could analyse if frames of a video when exposed to the the same CNN trained only by compressed single images could performthe same.
% >>>>>>>>>>>>>>>>>>>>>>>>>.


Often, h.264 and h.265 codecs are used for lossy compression but can also be used for lossless compression.
After the prediction step, these codecs use the Discrete Cosine Transform (DCT) to obtain coefficients in frequency domain and, then, they apply a quantization matrix to reduce data.
The factor of this quantization matrix controls the compression behaviour.
If the quantization factor is zero, then, all prediction errors are stored without reduction and, at decoding phase, they are used to reconstruct the video with no loss.
If the quantization factor is greater than zero, then, it is a lossy compression and, the higher the quantization factor is, the smaller will be the size and the overall quality of the resulting video.
Therefore, lossy compression increases the pixel-to-pixel error and can decrease perceptual quality when the quantization factor is high.


The FFmpeg software provides implementation of many video codecs and can control the bit rate of compressed videos using input parameters.
In this paper, we chose h.264 and h.265 codecs because FFmpeg has a uniform parameter, the Constant Rate Factor \texttt{CRF}, that controls the compression level of these codecs.
The \texttt{CRF} parameter for h.264 and h.265 varies from $0$ to $51$, where $0$ means the compression is lossless and $51$ means the compression has the highest loss.
The default value for \texttt{CRF} is $23$ and the documentation says that, in order to keep visually lossless quality, one should use \texttt{CRF} values near $17$ or $18$.
In this paper, we vary \texttt{CRF} from $1$ to $51$ using h.264 and h.265 for every video described in Section~\ref{sub:Video Acquisition}.
Below is an example of the command line we used to compress the video \texttt{ducks.y4m} to \texttt{ducks\_h264\_1.mp4} using the codec h.264 with \texttt{CRF} equals to $1$.

\begin{lstlisting}
$ ffmpeg -i ducks.y4m \
    -vcodec libx264 -crf 1 ducks_h264_1.mp4
\end{lstlisting}




\subsection{Frame Extraction}%
\label{sub:Frame Extraction}

We used the FFmpeg software to extract $10$ frames from each compressed video.
First, we queried the duration of the videos using the \texttt{ffprobe} command, a program included in the FFmpeg installation.
Below is an example of the command line we used to query the duration of the video \texttt{ducks\_h264\_1.mp4}.

\begin{lstlisting}
$ duration=$(ffprobe -i ducks_h264_1.mp4 \
    -show_entries format=duration \
    -v quiet -of csv="p=0")
\end{lstlisting}

Then, we generated $10$ time stamps at random ranging from $0$ to the duration of each video.
Finally, we extracted the next frame after the generated time stamps in each compressed video.
Note that the time stamps generated for a particular reference video were used to extract frames from all corresponding compressed videos.
Below is an example of the command line we used to extract the first frame after the second $5$ of the compressed video \texttt{ducks\_h264\_1.mp4} as the image \texttt{ducks\_h264\_1\_5.bmp}.

\begin{lstlisting}
$ ffmpeg -i ducks_h264_1.mp4 \
    -ss 5 -vframes 1 ducks_h264_1_5.bmp
\end{lstlisting}


\subsection{Perceptual Quality Inference}%
\label{sub:Perceptual Quality Inference}

After extraction, we evaluated the compressed video frames using the CNN models proposed by Bosse et al. in~\cite{journals/tip/BosseMMWS18}.
In their work, the authors built models from two datasets, TID2013~\cite{tid2013} and LIVE\cite{live}, using two different approaches, FR and NR, in which they compare CNNs with two different pooling layers applying standard mean or weighted mean.
In this paper, we will explore only the FR and NR models built from TID2013 dataset using weighted mean variant of pooling layer.

The original authors have made their code and models available on \url{https://github.com/dmaniry/deepIQA}.
In order to evaluate a compressed frame using the FR approach, the reference frame needs to be passed to their program.
Differently, for NR approach, only the compressed frame needs to be passed as input.
The output of all executions were stored in a Comma Separated Values (CSV) file to run the analysis described in Section~\ref{sec:experiment}.
Below is an example of the command line we used to execute the CNN model \texttt{fr\_tid\_weighted.model} to predict the perceptual quality of the compressed frame \texttt{ducks\_h264\_1\_5.bmp} using the reference \texttt{ducks\_reference\_5.bmp} for the FR approach.

\begin{lstlisting}
result=$(python evaluate.py \
    --model fr_tid_weighted.model --top weighted \
    ducks_h264_1_5.bmp ducks_reference_5.bmp)
\end{lstlisting}


Additionally, we also computed quality measurements of the compressed videos using the ImageMagick software~\cite{imagemagick}.
This software is often used to automate image edition but it also provides the ability to compute quality measurements of compressed images given the reference picture.
In this paper, we computed Peak Signal-to-Noise Ratio (PSNR) and Structural Similarity (SSIM) for every compressed frame using the corresponding frame of the reference video.
Below is an example of the command line we used to compute the PSNR of the compressed frame \texttt{ducks\_h264\_1\_5.bmp} regarding its reference frame \texttt{ducks\_reference\_5.bmp}.
The residuals are stored in the output image \texttt{residuals.png} and can be ignored.


\begin{lstlisting}
psnr=$(magick compare -metric psnr \
    ducks_h264_1_5.bmp ducks_reference_5.bmp \
    residuals.png 2>&1)
\end{lstlisting}


All scripts for data acquisition, video compression, frame extraction and evaluation are public available in our repository on \url{https://bitbucket.org/luizcoro/seminario-multimidia-2019/}.

\begin{table}[ht]
	\centering
	%\scriptsize
	\caption{Evaluated Parameters}
		\begin{tabular}{ll}
       \hline
            Data & Value \\
        \hline
			 Compression Codecs &  H.264 and H.265  \\
			 Quality Loss levels (CRF) & 0, 1, 6, 11, 16, 21, 26, 31, 36, 41, 46, 51 \\
            Assessment approaches & FR and NR \\
            Videos & 4 \\
            Frames Extracted per Video & 500 \\
            CNN Training model & TID2013 weighted \\
            Total of Frame Samples & 2000         \\

			\hline
	   \end{tabular}
	\label{tab:configurations}
\end{table}

\section{Results}%
\label{sec:experiment}

In this section, we present and discuss our results.
We gathered outputs of the CNN models proposed by Bosse et al. in~\cite{journals/tip/BosseMMWS18} along with the quality measures PSNR and SSIM.
% in order to compare perceptual quality and other well-known measures.
PSNR is based on pixel-to-pixel error of the compressed frames and SSIM is a measure that treats structural components differently in order to achieve a quality closer to our visual perception.
Therefore, the results of the CNN models should present more similarities with SSIM than PSNR.

Instead of presenting the results for each extracted frame, we opted to present in terms of mean and standard deviation.
This aggregation also generalize the experiments and improve the readability of graphs.
Additionally, we inverted the output $v$ from the CNN models, to analyse quality level instead of quality loss level, also to be able to compare them with the other measures in this same behavior. It was accomplished by using the function $v_{inv} = 100 - v$. The $100$ element in the function represents the highest possible output value $v$ from the model and originally meant the highest quality loss, while 0 was the lowest quality loss. After the inversion, at the graphs, 0 represents the lowest quality level, while 100, the highest quality level.



\begin{figure*}[!t]
    \centering
	\begin{tabular}{cc}
    \includegraphics[width=0.95\columnwidth]{images/fr-inv.png} & 
    \includegraphics[width=0.95\columnwidth]{images/nr-inv.png} \\
    (a) & (b) \\
    \includegraphics[width=0.95\columnwidth]{images/psnr.pdf} & 
    \includegraphics[width=0.95\columnwidth]{images/ssim.pdf} \\
    (c) & (d) \\
	\end{tabular}	
  \caption{Results for each video using h.264 and h.265 video codecs. In $x$ axis, we vary the values of \texttt{CRF} while, in $y$ axis, we present the results for the FR model (a), NR model (b), PSNR(c) and SSIM (d). Each line describes the different videos that we previously presented in Figure~\ref{fig:videos}.} \label{fig:results}
\end{figure*}


%\begin{figure}
%  \subfloat[]{%
%  \begin{minipage}{\linewidth}
%  \includegraphics[width=.45\linewidth]{images/frm-inv.pdf}\hfill
%  \includegraphics[width=.45\linewidth]{images/frm-inv.pdf}%
%  \end{minipage}%
%  }\par
%  \subfloat[]{%
%  \begin{minipage}{\linewidth}
%  \includegraphics[width=.45\linewidth]{images/psnr.pdf}\hfill
%  \includegraphics[width=.45\linewidth]{images/nrm-inv.pdf}%
%  \end{minipage}%
%  }\par
%  \caption{Results for each video using h.264 and h.265 video codecs. In $x$ axis, we vary the values of \texttt{CRF} while, in $y$ axis, we present the results for the FR model (a), NR model (b), PSNR(c) and SSIM (d). Each line describes the different videos that we previously presented in Figure~\ref{fig:videos}.}
%\end{figure}

In Figure~\ref{fig:results}, we present the results of the models FR and NR, and the measures PSNR and SSIM, varying values of \texttt{CRF}.
We noted that the FR model describes more accurately the perceptual quality of the compressed videos than PSNR.
In our experiments, PSNR had approximately a constant decreasing behaviour for all videos as the \texttt{CRF} increased and, therefore, do not corresponds well to our visual perception.
Differently, the FR model presents a non-linear descending curve showing that the perceptual quality of compressed videos does not decrease at the same rate.
% After a threshold value of \texttt{CRF}, the FR model decreases the perceptual quality at a steady rate, surpassing values of PSNR.
This behaviour seems more natural to our visual perception since small degradations sometimes are not captured by our visual system.
Furthermore, when the video begins to present distortions, as \texttt{CRF} values increases, our visual system begins to perceive the decreasing of quality more clearly, which agrees with the FR model. 

Its important to notice that the FR value presents a larger standard deviation as \texttt{CRF} increases, especially for \texttt{park} and \texttt{house} videos. These videos present more details and movement, which affect negatively the compression. Differently, PNSR presents larger standard deviation for smaller values of \texttt{CRF}, thus corroborating its inability to correctly quantify the perceptual quality of the video. 

Another important aspect is that the results of FR model have more similarities with SSIM than PSNR, even though they do not agree precisely.
This is expected as the SSIM measure captures more characteristics of our visual system than PSNR does.
% In fact, in Figure~\ref{fig:house}, we note that SSIM was closer to PSNR, thus, we believe the results obtained by the FR model are closer to our visual perception of quality.

In contrast, the NR model did not exhibit an accurate description of our visual perception in most videos.
This is due to the absence of information about reference frames, which compromises the ability of the method to infer the perceptual quality. For example, in the video \texttt{ducks}, the NR model obtained very low fluctuation as the \texttt{CRF} value increased.
% Observing the frames, we noted that there were no pictures containing ducks, only water.
We believe that this behaviour happened because the frames were very similar.
As we compress the video, it is expected its perceptual quality to diminish. Yet, it can be observed that the curves representing \texttt{park} and \texttt{town} videos, in Figure~\ref{fig:results}b, showed an unexpected oscillation (as also a large standard deviation), with exception of the video \texttt{town} compressed with h.265. However, in \texttt{house}, the NR model was very close to the FR model and described the perceptual quality of the compressed video more accurately. Such result may be due to the presence of reference in the training process.
Therefore, there are cases in which the NR model can be used to infer the human perception of quality in compressed videos.
Further investigation is needed to point the cases that the usage of NR model is appropriate.

According to FFmpeg documentation, \texttt{CRF} values around $17$-$18$ were expected to generate compression without quality loss perceivable by our visual system.
However, as our results show, this threshold appears to be around $25$-$26$ when using the FR model, for the videos presented in this paper.
Therefore, more aggressive compressions can be used, saving space and, consequently, improving transmission.


In this paper, we do not show the sequences of videos to compare with the results.
We suggest running the scripts publicly available in our repository on \url{https://bitbucket.org/luizcoro/seminario-multimidia-2019/} to have access and reproduce the compressed videos.


\section{Conclusions}
\label{sec:conclusion}

In this work we evaluated the results of the methods created by Bosse et al. in \cite{journals/tip/BosseMMWS18}, using various frames as test samples extracted from several compressed videos. For this work, we used four raw videos. We generated different quality levels of compression for each video. We also utilized h.264 and h.265 codec compression in order to explore the effects of the loss levels in the result of the automatic evaluation.

In terms of NR assessment of images, it can be noticed that the results is sometimes equivocated, as the methods suggest that visual perception alternates sometimes between lower and higher values, even though the quality in our tests only decreases. Also, as shown in Figure \ref{fig:results}b, the algorithm predicted a kind of uniform level of quality despite the constant decreasing of compression quality. We believe that it is due to the fact that that video has very similar frames.

In contrast, results also demonstrated that the proposed methods of Image Quality Assessment using Deep CNN has a great effectiveness in most cases when using FR approach.
Despite the CNN models have only been trained with single pictures, exploiting only spatial redundancies, FR method was able to infer perceptual quality on compressed video frames.
% In particular, when the \texttt{CRF} parameter was greater then about $25$, we could observe the first distortions while reproducing the compressed videos and our results using the FR model agreed, as we  in Figure~\ref{fig:results}.
Indicating that the approach covered in this paper can be considered a feasible solution for IQA of video frames specially in FR approach.
As future work, we propose to investigate further the cases in which the usage of NR model is appropriate.

\section*{Acknowledgments}

Andr\'e R. Backes gratefully acknowledges the financial support of CNPq (National Council for Scientific and Technological Development, Brazil) (Grant \#301715/2018-1). This study was financed in part by the Coordena\c{c}\~ao de Aperfei\c{c}oamento de Pessoal de N\'ivel Superior - Brazil (CAPES) - Finance Code 001.

\bibliographystyle{IEEEtran}
\bibliography{referencias}

\vspace{12pt}

\end{document}
