#!/bin/bash

mkdir -p data/ducks
echo "Downloading ducks..."
curl https://media.xiph.org/video/derf/y4m/ducks_take_off_444_720p50.y4m --output data/ducks/reference.y4m

mkdir -p data/house
echo -e "\nDownloading house..."
curl https://media.xiph.org/video/derf/y4m/in_to_tree_444_720p50.y4m --output data/house/reference.y4m

mkdir -p data/town
echo -e "\nDownloading town..."
curl https://media.xiph.org/video/derf/y4m/old_town_cross_444_720p50.y4m --output data/town/reference.y4m

mkdir -p data/park
echo -e "\nDownloading park..."
curl https://media.xiph.org/video/derf/y4m/park_joy_444_720p50.y4m --output data/park/reference.y4m
