# Systems for Multimedia Processing -- 2019 -- UFU

This repository implements simple scripts to download reference videos, compress them, extract random frames, and evaluate the [deepIQA](https://github.com/ma3252788/Deep-Neural-Networks-for-No-Reference-and-Full-Reference-Image-Quality-Assessment/tree/961831181e196399140089afbea2abe91c5ac23f/) technique.

We first downloaded four 720p reference videos with 4:4:4 sampling from [xiph.org](https://media.xiph.org/video/derf/). Then we encoded them to h.264 and h.265 in three different qualities (-crf 10/25/40) using the [FFmpeg](https://ffmpeg.org/) sofware.
After, we extracted 5 frames from each video randomly, where frames from each coded video corresponds to the frames of the reference video.
Finnaly, we evaluate the [deepIQA](https://github.com/ma3252788/Deep-Neural-Networks-for-No-Reference-and-Full-Reference-Image-Quality-Assessment/tree/961831181e196399140089afbea2abe91c5ac23f/) technique passing as input the extracted frames using two approaches: Full Reference (FR) and No Reference (NR).
The models used to evaluate FR and NR are `authors-code/models/fr_tid_weighted.model` and `authors-code/models/nr_tid_weighted.model`.

Results for FR and NR are `fr-plot.jpeg` and `nr-plot.jpeg`.

* __Authors__: Luiz Fernando Afra Brito and Renato Rodrigues da Silva
* __Professor__: Prof. Dr. Marcelo Zanchetta do Nascimento


## Dependencies

To run this repository you will need the following:

* the same dependencies listed in [deepIQA](https://github.com/ma3252788/Deep-Neural-Networks-for-No-Reference-and-Full-Reference-Image-Quality-Assessment/tree/961831181e196399140089afbea2abe91c5ac23f/)
* support for [Bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)), to run Bash scripts
* [curl](https://curl.haxx.se/), to download the reference videos
* [FFmpeg](https://ffmpeg.org/), to compress and extract frames
* [ImageMagick](https://imagemagick.org/index.php), to compute error metrics
* [R](https://www.r-project.org/) with [ggplot2](https://ggplot2.tidyverse.org/) and [ggthemes](https://yutannihilation.github.io/allYourFigureAreBelongToUs/ggthemes/), to run plot.r


## Evaluation Steps

These are the command lines to evaluate the [deepIQA](https://github.com/ma3252788/Deep-Neural-Networks-for-No-Reference-and-Full-Reference-Image-Quality-Assessment/tree/961831181e196399140089afbea2abe91c5ac23f/) technique using the h.264 and h.265 encoders:

* `git submodule update --init`
* `./evaluate.sh data/* fr`
* `./evaluate.sh data/* nr`
* `./plot.r fr-results.csv nr-results.csv fr` (will create the plots


## Evaluation from Scratch

In case you need to do the whole proccess, these are the command lines:

* `git submodule update --init`
* `rm -rf data/*`
* `./download.sh`
* `./compress.sh data/*`
* `./get-frames.sh data/* NumberOfFrames`
* `./evaluate.sh data/* fr`
* `./evaluate.sh data/* nr`
* `./plot.r fr-results.csv nr-results.csv fr` (will create the plots
