#!/usr/bin/env Rscript

suppressMessages(library(ggplot2))
suppressMessages(library(ggthemes))
suppressMessages(library(reshape2))
suppressMessages(library(plyr))

#' @title ggplot2 theme for scientific publications
#'
#' @description
#' Themes set the general aspect of the plot such as the color of the
#' background, gridlines, the size and colour of fonts. This particular theme is
#' based on the classic dark-on-light ggplot2 \code{theme_bw} and has been used
#' for scientific publications,
#'
#' @param base_size base font size
#' @param base_family base font family
#' @param line_size base line size for, e.g. for ticks and axes
#' @param \dots further arguments to be passed to \code{theme_bw}
#'
#' @examples
#' require(ggplot2)
#' p <- ggplot(mtcars) + geom_point(aes(x = wt, y = mpg,
#'      colour=factor(gear))) + facet_wrap( ~ am)
#' p
#' p + theme_publish()
#'
#' @seealso
#' \code{\link[ggplot2]{ggtheme}}
#'
#' @import ggplot2
#' @export
theme_publish <- function(base_size = 12, base_family = "",
                          line_size = 0.25, ...) {
  half_line <- base_size / 2
  small_rel <- 0.875
  small_size <- small_rel * base_size

  theme_bw(base_size = base_size, base_family = base_family, ...) %+replace%
    theme(
      rect = element_rect(fill = "transparent", colour = NA, color = NA,
                          size = 0, linetype = 0),
      text = element_text(family = base_family, face = "plain",
                          colour = "black", size = base_size, hjust = 0.5,
                          vjust = 0.5, angle = 0, lineheight = 0.9,
                          margin = ggplot2::margin(), debug = F),

      axis.text = element_text(size = small_size),
      axis.text.x = element_text(margin = ggplot2::margin(t = small_size/4),
                                 vjust = 1),
      axis.text.y = element_text(margin = ggplot2::margin(r = small_size/4),
                                 hjust = 1),
      axis.title.x = element_text(margin = ggplot2::margin(t = small_size,
                                                           b = small_size)),
      axis.title.y = element_text(angle = 90,
                                  margin = ggplot2::margin(r = small_size,
                                                           l = small_size/4)),
      axis.ticks = element_line(colour = "black", size = line_size),
      axis.ticks.length = unit(0.25, 'lines'),

      axis.line = element_line(colour = "black", size = line_size),
      axis.line.x = element_line(colour = "black", size = line_size),
      axis.line.y = element_line(colour = "black", size = line_size),

      legend.spacing = unit(base_size/4, "pt"),
      legend.key = element_blank(),
      legend.key.size = unit(1 * base_size, "pt"),
      legend.key.width = unit(1.5 * base_size, 'pt'),
      legend.text = element_text(size = rel(small_rel)),
      legend.title = element_text(size = rel(small_rel), face = 'bold'),
      legend.position = 'bottom',
      legend.box = 'horizontal',
      legend.box.spacing = unit(-5, "pt"),


      panel.spacing = unit(1, "lines"),
      panel.background = element_blank(),
      panel.border = element_blank(),
      panel.grid.major = element_blank(),
      panel.grid.minor = element_blank(),

      strip.text = element_text(size = base_size),
      strip.background = element_rect(fill = NA, colour = "black", size = 0.125),
      strip.text.x = element_text(face = 'bold', hjust = 0,
                                  margin = ggplot2::margin(b = small_size/2,
                                                           t = small_size/4)),
      strip.text.y = element_text(angle = -90, face = 'bold',
                                  margin = ggplot2::margin(l = small_size/2,
                                                           r = small_size/4)),

      plot.margin = unit(c(5,2.5,0,0), "pt"),
      plot.background = element_blank(),
      plot.title = element_text(face = "bold", size = 1.125 * base_size,
                                margin = ggplot2::margin(b = half_line))
    )
}

args <- commandArgs(trailingOnly = TRUE)
fr_filename <- args[1]
nr_filename <- args[2]
output_dir <- args[3]

if (substr(output_dir, nchar(output_dir), nchar(output_dir)) != "/")
    output_dir <- paste0(output_dir, "/")

cat("Loading files\n")

frd <- read.csv(fr_filename)
nrd <- read.csv(nr_filename)

cat("Preprocessing data\n")

mfrd <- melt(frd, c("video", "codec", "quality", "frame"))
mnrd <- melt(nrd, c("video", "codec", "quality", "frame"))
md <- rbind(mfrd, mnrd)

ref_indexes <- md$codec == "reference"
refs <- md[ref_indexes, ]
md <- md[!ref_indexes, ]

md$video <- factor(md$video, levels = c("ducks", "house", "park", "town"),
                  labels = c("  ducks    ", "  house    ",
                             "  park    ", "  town    "))
md$codec <- factor(md$codec, levels = c("libx264", "libx265", "reference"),
                  labels = c("H.264", "H.265", "reference"))

mdp <- ddply(md, .(video, codec, quality, variable),
             summarize, mean = mean(value), sd = sd(value))
mdpm <- melt(mdp, id = c("video", "codec", "quality", "variable"),
             variable.name = "variable2")
d <- dcast(mdpm, video + codec + quality ~ variable + variable2)



# legend_labels <- c("  FRM    ",
#                    "  NRM    ",
#                    "  SSIM    ",
#                    "  PSNR    ")

# ggplot(d, aes(quality)) +
#     geom_line(aes(y = y[, "mean"], color = legend_labels[1])) +
#     geom_errorbar(aes(ymin = y[, "mean"] - y[, "sd"],
#                       ymax = y[, "mean"] + y[, "sd"],
#                       color = legend_labels[1]),
#                   width = .2) +
#     geom_line(aes(y = y2[, "mean"], color = legend_labels[2])) +
#     geom_errorbar(aes(ymin = y2[, "mean"] - y2[, "sd"],
#                       ymax = y2[, "mean"] + y2[, "sd"],
#                       color = legend_labels[2]),
#                   width = .2) +
#     geom_line(aes(y = ssim[, "mean"], color = legend_labels[3])) +
#     geom_errorbar(aes(ymin = ssim[, "mean"] - ssim[, "sd"],
#                       ymax = ssim[, "mean"] + ssim[, "sd"],
#                       color = legend_labels[3]),
#                   width = .2) +
#     geom_line(aes(y = psnr[, "mean"] * 10, color = legend_labels[4])) +
#     geom_errorbar(aes(ymin = psnr[, "mean"] * 10 - psnr[, "sd"] * 10,
#                       ymax = psnr[, "mean"] * 10 + psnr[, "sd"] * 10,
#                       color = legend_labels[4]),
#                   width = .2) +
#     scale_y_continuous(limits = c(0, 10),
#                        sec.axis = sec_axis(~ . * max_psnr / 10,
#                                            name = "PSNR")) +
#     facet_grid(video ~ codec) +
#     xlab("CRF") + ylab("MOS") +
#     theme_publish() +
#     theme(legend.position = "bottom",
#           legend.title = element_blank()) +
#     scale_colour_colorblind(breaks = legend_labels)

# ggsave(paste0(output_dir, "together-plots.pdf"))


# ggplot(d[d$video == "ducks", ], aes(quality)) +
#     geom_line(aes(y = y[, "mean"], color = legend_labels[1])) +
#     geom_errorbar(aes(ymin = y[, "mean"] - y[, "sd"],
#                       ymax = y[, "mean"] + y[, "sd"],
#                       color = legend_labels[1]),
#                   width = .2) +
#     geom_line(aes(y = y2[, "mean"], color = legend_labels[2])) +
#     geom_errorbar(aes(ymin = y2[, "mean"] - y2[, "sd"],
#                       ymax = y2[, "mean"] + y2[, "sd"],
#                       color = legend_labels[2]),
#                   width = .2) +
#     geom_line(aes(y = ssim[, "mean"], color = legend_labels[3])) +
#     geom_errorbar(aes(ymin = ssim[, "mean"] - ssim[, "sd"],
#                       ymax = ssim[, "mean"] + ssim[, "sd"],
#                       color = legend_labels[3]),
#                   width = .2) +
#     geom_line(aes(y = psnr[, "mean"] * 10, color = legend_labels[4])) +
#     geom_errorbar(aes(ymin = psnr[, "mean"] * 10 - psnr[, "sd"] * 10,
#                       ymax = psnr[, "mean"] * 10 + psnr[, "sd"] * 10,
#                       color = legend_labels[4]),
#                   width = .2) +
#     scale_y_continuous(limits = c(0, 10),
#                        sec.axis = sec_axis(~ . * max_psnr / 10,
#                                            name = "PSNR")) +
#     facet_grid(~ codec) +
#     xlab("CRF") + ylab("MOS") +
#     theme_publish() +
#     theme(legend.position = "bottom",
#           legend.title = element_blank()) +
#     scale_colour_colorblind(breaks = legend_labels)

# ggsave(paste0(output_dir, "together-ducks-plot.pdf"),
#        width = 12, height = 8, units = "cm")

# ggplot(d[d$video == "house", ], aes(quality)) +
#     geom_line(aes(y = y[, "mean"], color = legend_labels[1])) +
#     geom_errorbar(aes(ymin = y[, "mean"] - y[, "sd"],
#                       ymax = y[, "mean"] + y[, "sd"],
#                       color = legend_labels[1]),
#                   width = .2) +
#     geom_line(aes(y = y2[, "mean"], color = legend_labels[2])) +
#     geom_errorbar(aes(ymin = y2[, "mean"] - y2[, "sd"],
#                       ymax = y2[, "mean"] + y2[, "sd"],
#                       color = legend_labels[2]),
#                   width = .2) +
#     geom_line(aes(y = ssim[, "mean"], color = legend_labels[3])) +
#     geom_errorbar(aes(ymin = ssim[, "mean"] - ssim[, "sd"],
#                       ymax = ssim[, "mean"] + ssim[, "sd"],
#                       color = legend_labels[3]),
#                   width = .2) +
#     geom_line(aes(y = psnr[, "mean"] * 10, color = legend_labels[4])) +
#     geom_errorbar(aes(ymin = psnr[, "mean"] * 10 - psnr[, "sd"] * 10,
#                       ymax = psnr[, "mean"] * 10 + psnr[, "sd"] * 10,
#                       color = legend_labels[4]),
#                   width = .2) +
#     scale_y_continuous(limits = c(0, 10),
#                        sec.axis = sec_axis(~ . * max_psnr / 10,
#                                            name = "PSNR")) +
#     facet_grid(~ codec) +
#     xlab("CRF") + ylab("MOS") +
#     theme_publish() +
#     theme(legend.position = "bottom",
#           legend.title = element_blank()) +
#     scale_colour_colorblind(breaks = legend_labels)

# ggsave(paste0(output_dir, "together-house-plot.pdf"),
#        width = 12, height = 8, units = "cm")


# ggplot(d[d$video == "park", ], aes(quality)) +
#     geom_line(aes(y = y[, "mean"], color = legend_labels[1])) +
#     geom_errorbar(aes(ymin = y[, "mean"] - y[, "sd"],
#                       ymax = y[, "mean"] + y[, "sd"],
#                       color = legend_labels[1]),
#                   width = .2) +
#     geom_line(aes(y = y2[, "mean"], color = legend_labels[2])) +
#     geom_errorbar(aes(ymin = y2[, "mean"] - y2[, "sd"],
#                       ymax = y2[, "mean"] + y2[, "sd"],
#                       color = legend_labels[2]),
#                   width = .2) +
#     geom_line(aes(y = ssim[, "mean"], color = legend_labels[3])) +
#     geom_errorbar(aes(ymin = ssim[, "mean"] - ssim[, "sd"],
#                       ymax = ssim[, "mean"] + ssim[, "sd"],
#                       color = legend_labels[3]),
#                   width = .2) +
#     geom_line(aes(y = psnr[, "mean"] * 10, color = legend_labels[4])) +
#     geom_errorbar(aes(ymin = psnr[, "mean"] * 10 - psnr[, "sd"] * 10,
#                       ymax = psnr[, "mean"] * 10 + psnr[, "sd"] * 10,
#                       color = legend_labels[4]),
#                   width = .2) +
#     scale_y_continuous(limits = c(0, 10),
#                        sec.axis = sec_axis(~ . * max_psnr / 10,
#                                            name = "PSNR")) +
#     facet_grid(~ codec) +
#     xlab("CRF") + ylab("MOS") +
#     theme_publish() +
#     theme(legend.position = "bottom",
#           legend.title = element_blank()) +
#     scale_colour_colorblind(breaks = legend_labels)

# ggsave(paste0(output_dir, "together-park-plot.pdf"),
#        width = 12, height = 8, units = "cm")


# ggplot(d[d$video == "town", ], aes(quality)) +
#     geom_line(aes(y = y[, "mean"], color = legend_labels[1])) +
#     geom_errorbar(aes(ymin = y[, "mean"] - y[, "sd"],
#                       ymax = y[, "mean"] + y[, "sd"],
#                       color = legend_labels[1]),
#                   width = .2) +
#     geom_line(aes(y = y2[, "mean"], color = legend_labels[2])) +
#     geom_errorbar(aes(ymin = y2[, "mean"] - y2[, "sd"],
#                       ymax = y2[, "mean"] + y2[, "sd"],
#                       color = legend_labels[2]),
#                   width = .2) +
#     geom_line(aes(y = ssim[, "mean"], color = legend_labels[3])) +
#     geom_errorbar(aes(ymin = ssim[, "mean"] - ssim[, "sd"],
#                       ymax = ssim[, "mean"] + ssim[, "sd"],
#                       color = legend_labels[3]),
#                   width = .2) +
#     geom_line(aes(y = psnr[, "mean"] * 10, color = legend_labels[4])) +
#     geom_errorbar(aes(ymin = psnr[, "mean"] * 10 - psnr[, "sd"] * 10,
#                       ymax = psnr[, "mean"] * 10 + psnr[, "sd"] * 10,
#                       color = legend_labels[4]),
#                   width = .2) +
#     scale_y_continuous(limits = c(0, 10),
#                        sec.axis = sec_axis(~ . * max_psnr / 10,
#                                            name = "PSNR")) +
#     facet_grid(~ codec) +
#     xlab("CRF") + ylab("MOS") +
#     theme_publish() +
#     theme(legend.position = "bottom",
#           legend.title = element_blank()) +
#     scale_colour_colorblind(breaks = legend_labels)

# ggsave(paste0(output_dir, "together-town-plot.pdf"),
#        width = 12, height = 8, units = "cm")


# measures <- c("y", "y2", "ssim", "psnr")

# for (i in 1:length(measures)) {

# ggplot(d, aes(quality, y[, "mean"], colour = factor(video))) +
#     geom_line() +
#     geom_errorbar(aes(ymin = y[, "mean"] - y[, "sd"],
#                       ymax = y[, "mean"] + y[, "sd"]),
#                   width = .2) +
#     facet_grid(type ~ codec) +
#     xlab("CRF") + ylab("Prediction") +
#     theme_publish() +
#     theme(legend.position = "bottom",
#           legend.title = element_blank()) +
#     scale_colour_colorblind()

# cat(paste0("Saving ", output_dir,  "frm-nrm.pdf\n"))
# suppressMessages(ggsave(paste0(output_dir, "frm-nrm.pdf")))

for (y in c("rmse", "ssim", "psnr", "frm", "nrm")) {

    y_mean <- paste0(y, "_mean")
    y_min <- paste0(y, "_mean - ", y, "_sd")
    y_max <- paste0(y, "_mean + ", y, "_sd")

    ggplot(d, aes_string("quality", y_mean, colour = "factor(video)")) +
        geom_line() +
        geom_errorbar(aes_string(ymin = y_min, ymax = y_max),
                      width = .2) +
        facet_grid(~ codec) +
        xlab("CRF") + ylab(toupper(y)) +
        theme_publish() +
        theme(legend.position = "bottom",
              legend.title = element_blank()) +
        scale_colour_colorblind()

    cat(paste0("Saving ", output_dir, y, ".pdf\n"))
    suppressMessages(ggsave(paste0(output_dir, y, ".pdf"), width = 6, height = 4))
}


for (y in c("frm", "nrm")) {


    y_mean <- paste0(y, "_mean")
    y_min <- paste0(y, "_mean - ", y, "_sd")
    y_max <- paste0(y, "_mean + ", y, "_sd")

    d[, y_mean] <- 100 - d[, y_mean]

    ggplot(d, aes_string("quality", y_mean, colour = "factor(video)")) +
        geom_line() +
        geom_errorbar(aes_string(ymin = y_min, ymax = y_max),
                      width = .2) +
        facet_grid(~ codec) +
        xlab("CRF") + ylab(toupper(y)) +
        theme_publish() +
        theme(legend.position = "bottom",
              legend.title = element_blank()) +
        scale_colour_colorblind()

    cat(paste0("Saving ", output_dir, y, "-inv.pdf\n"))
    suppressMessages(ggsave(paste0(output_dir, y, "-inv.pdf"), width = 6, height = 4))
}
